package sda.spring.service;

import org.springframework.stereotype.Component;

@Component
public class Consumer {

	public void consumeMessage(){
		System.out.println("Consuming message...");
		for (int i = 0 ; i < 5 ; i++){
			Integer item = Launcher.QUEUE.poll();
			System.out.println("just read item == " + item);			
		}
	}
}
