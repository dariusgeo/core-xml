package sda.spring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import sda.spring.service.Launcher;

public class Main {

	public static void main(String[] args) {
		
		ApplicationContext context = new ClassPathXmlApplicationContext("classpath:spring/applicationContext.xml");
		Launcher launcher = context.getBean(Launcher.class);
		//Launcher launcher = (Launcher)context.getBean("launcher");
		
		launcher.execute();
		//context.publishEvent(event);
	}

}
